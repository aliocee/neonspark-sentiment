# [Sentiment Analysis Dashboard](http://http://www.neonspark.fi/sentiment) [![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]

> Sentiment analysis dashboard with instant feedback on webapp

This project is based on services provided by [NeonSpark](http://http://www.neonspark.fi)
designed with latest technology infrustructure for data scientst. 

Check the [Live application here](http://http://www.neonspark.fi/sentiment).

![](static/Dashboard.PNG)

[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE.md
[version-badge]: https://img.shields.io/badge/version-1.0.0-blue.svg
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
